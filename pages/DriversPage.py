from selene.api import *
from pages.BasePage import BasePage


class DriversPage(BasePage):
    def __init__(self):
        BasePage.__init__(self)
        self.add_driver_button = s('body > div.content > div > div.row.toolbar > div > a')

    def open(self):
        browser.open_url('drivers/')