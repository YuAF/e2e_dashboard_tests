from selene.api import *
from pages.BasePage import BasePage


class SearchPage(BasePage):
    def __init__(self):
        BasePage.__init__(self)
        self.orders_ids = ss(by.text('Load ID:'))
        self.first_assing_button = ss(by.partial_text('ssign')).first()

    def open(self, query):
        browser.open_url('orders/search/?q=' + query)

    def reassing_order(self):
        self.first_assing_button.click()
        ss(by.text('Unassign')).first().click()
        s('body > div.bootbox.modal.fade.bootbox-confirm.in > div > div > '
          'div.modal-footer > button.btn.btn-danger').click()