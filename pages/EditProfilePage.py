from selene.api import *
from pages.BasePage import BasePage


class EditProfilePage(BasePage):
    def __init__(self):
        BasePage.__init__(self)
        self.edit_profile_header = s('body > div.content > div > div.row > div > h4')

    def open(self):
        browser.open_url('edit/profile/')