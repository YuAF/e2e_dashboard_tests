from selene.api import *
from pages.BasePage import BasePage


class ReportsPage(BasePage):
    def __init__(self):
        BasePage.__init__(self)
        self.export_header = s('body > div.content > div > div.row > div.col-md-6.col-md-offset-1 > h4')

    def open(self):
        browser.open_url('orders/export/')