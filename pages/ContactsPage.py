from selene.api import *
from pages.BasePage import BasePage


class ContactsPage(BasePage):
    def __init__(self):
        BasePage.__init__(self)
        self.contact_name = s('body > div.content > div > div:nth-child(3) > div > div.contacts > div > div > h5')

    def open(self):
        browser.open_url('contacts/')

    def search_contact(self, contact_name):
        s('body > div.content > div > div.row.toolbar > div.col-md-4 > div > form > input').set_value(contact_name)
        s('body > div.content > div > div.row.toolbar > div.col-md-4 > div > form > button').click()

    def remove_contact(self):  # needs to be changed if for cases with 2+ contats on the page
        s(by.text('Remove')).click()
        s('body > div.bootbox.modal.fade.bootbox-confirm.in '
          '> div > div > div.modal-footer > button.btn.btn-primary').should(be.clickable).click()
        # s('body > div.bootbox.modal.fade.bootbox-confirm.in '
        #   '> div > div > div.modal-footer > button.btn.btn-primary').should(be.hidden)
