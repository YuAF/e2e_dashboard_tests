from selene.api import *
from pages.BasePage import BasePage
from selene.browser import driver
import time


class EditOrderPage(BasePage):
    def __init__(self):
        BasePage.__init__(self)
        self.order_activity_tab = s('#order-form > div > div.col-md-3 > div:nth-child(2)')
        self.green_popup = s('body > div.content > div > div.notifications.top-right > div')
        self.load_id_input = s('#id_number')
        self.mark_as_payed_button = s('#order-form > div > div.col-md-3 > div:nth-child(1) > p:nth-child(3) > button')
        self.unarchive_button = s('#order-form > div > div.col-md-3 > div:nth-child(1) > p:nth-child(3) > a')

    def enter_load_id(self, load_id):
        self.load_id_input.set_value(load_id)

    def fill_vehicle_info(self, vin, year, maker, model, inop=None, type=None, id='0'):
        if inop:
            s('#id_vehicles-'+id+'-is_inoperable').click()
        if type:
            s('#id_vehicles-' + id + '-type').send_keys(type)
        s('#id_vehicles-'+id+'-vin').set_value(vin)
        s('#id_vehicles-'+id+'-year').set_value(year)
        s('#id_vehicles-'+id+'-maker').set_value(maker)
        s('#id_vehicles-'+id+'-model').set_value(model)
        s('#id_vehicles-'+id+'-type').send_keys()

    def fill_delivery_address(self, address):
        s('#id_delivery_address').set_value(address)

    def fill_pickup_address(self, address):
        s('#id_pickup_address').set_value(address)

    def save_order(self):
        s('#order-form > div > div.col-md-3 > div:nth-child(1) > p:nth-child(1) > input').should(be.enabled).click()

    def select_driver(self, driver):
        s(by.text(driver)).click()

    def back_to_orders(self):
        s('body > div.content > div > div.row > div > ol > li:nth-child(1) > a').click()

    def add_vehicle(self):
        s(by.text('Add vehicle')).click()

    def set_status(self, status):
        s(by.text(status)).click()

    def set_price(self, price):
        s('#id_price').set_value(price)

    def send_bol(self, email):
        s(by.text('Send BOL')).click()
        # s('#bol-email').should(be.clickable).set_value(email)
        s('#bol-email').set_value(email)
        s('#bol-email').should(have.value(email))
        s('#send-bol-btn').click()
        time.sleep(2)  # todo remove sleep
        driver().switch_to.alert.accept()
        s('body > div.modal-backdrop.fade').should_not(be.enabled)

    def delivery_bill_client(self):
        s('#id_delivery_bill_client').click()

    def save_contact(self):
        s('#id_save_broker_contact').click()

    def enter_shipper_name(self, name):
        s('#id_broker_name').set_value(name)

    def mark_as_payed_and_cancel(self):
        self.mark_as_payed_button.click()
        s('#mark_paid_form > div.modal-footer > button.btn.btn-default.btn-sm').should(be.clickable).click()
        s('body > div.modal-backdrop.fade').should_not(be.enabled)

    def archive(self):
        s('#order-form > div > div.col-md-3 > div:nth-child(1) > p:nth-child(3) > a').should(be.enabled).click()