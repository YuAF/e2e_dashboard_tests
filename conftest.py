import pytest
from selene.api import *
from selene import config
from selene.browsers import BrowserName
from pages.LoginPage import LoginPage


@pytest.fixture(scope='module')
def open_browser():
    config.browser_name = BrowserName.CHROME
    config.start_maximized = True
    config.timeout = 10
    config.base_url = 'https://dashboard.mysuperdispatch.com/dashboard/'


@pytest.fixture(scope='module')
def login(open_browser):
    LoginPage().open()
    LoginPage().login('s.ivanov@mobidev.biz', 'dispatcher')
    yield
    browser.quit()
