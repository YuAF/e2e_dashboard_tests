import pytest
import re
import allure
from selene.api import *
from pages.LoginPage import LoginPage
from pages.OrdersPage import OrdersPage
from pages.EditOrderPage import EditOrderPage
from pages.SearchPage import SearchPage
from pages.BasePage import BasePage
from pages.ViewOrderPage import ViewOrderPage
from pages.ContactsPage import ContactsPage
from pages.DeletedOrdersPage import DeletedOrdersPage


class OrderData:
    load_id = '777Load_id_test'
    vin = '777VIN_test'
    year = '2017'
    maker = '777make_test'
    model = '777model_test'
    pickup_address = '777test_address'
    delivery_address = '888test_address'
    initial_driver_name = 'Bruce Willis'
    changed_driver_name = 'Ben Affleck'
    second_vin = '888IN_test'
    second_year = '2000'
    second_maker = '888make_test'
    second_model = '888model_test'
    second_type = 'a'  # for ATV
    price = '7777'
    bol_email = 'te@st.te'
    shipper_name = 'shipper 777'
    payed_amount = '717171'
    payment_method = 'd'  # for Direct Deposit


@pytest.fixture(scope='class')
def create_blank_order(login):
    OrdersPage().open_n_create()
    url = browser.driver().current_url
    order_id = re.findall(r'\d+', url)[0]  # parsing url and getting order ID from it
    allure.attach('order_id', order_id)
    yield order_id  # returns order ID for using in tests
    browser.open_url('orders/'+str(order_id)+'/delete/')  # once all tests are finished - will delete created order


class TestOrders:

    def test_create_order(self, create_blank_order):
        EditOrderPage().enter_load_id(OrderData.load_id)
        EditOrderPage().fill_vehicle_info(OrderData.vin, OrderData.year, OrderData.maker, OrderData.model)
        EditOrderPage().fill_pickup_address(OrderData.pickup_address)
        EditOrderPage().select_driver(OrderData.initial_driver_name)
        EditOrderPage().save_order()
        EditOrderPage().green_popup.should(have.text('Load #'+OrderData.load_id+' saved'))

    def test_back_to_orders(self, create_blank_order):
        EditOrderPage().back_to_orders()
        OrdersPage().create_order_button.should(have.exact_text('Create Order'))

    def test_search_order(self, create_blank_order):
        OrdersPage().search_order(OrderData.load_id)
        SearchPage().orders_ids.first().should(have.text(OrderData.load_id))

    def test_reassing_order(self, create_blank_order):
        SearchPage().reassing_order()
        SearchPage().first_assing_button.should(have.text('Assign'))

    def test_sign_out(self, create_blank_order):
        BasePage().sign_out()
        LoginPage().header.should(have.exact_text('Dashboard (Super Dispatch)'))

    def test_sign_in_as_other(self, create_blank_order):
        LoginPage().login('s.ivanov2@mobidev.biz', 'mobidev')
        LoginPage().header.should(have.text('Sign Out'))

    def test_click_edit_order(self, create_blank_order):
        OrdersPage().edit_order(create_blank_order)
        EditOrderPage().load_id_input.should(have.value(OrderData.load_id))

    def test_edit_order(self, create_blank_order):
        EditOrderPage().select_driver(OrderData.changed_driver_name)
        EditOrderPage().add_vehicle()
        EditOrderPage().fill_vehicle_info(OrderData.second_vin, OrderData.second_year,
                                          OrderData.second_maker, OrderData.second_model,
                                          inop=True, type=OrderData.second_type, id='1')
        EditOrderPage().save_order()
        EditOrderPage().green_popup.should(have.text('Load #' + OrderData.load_id + ' saved'))

    def test_save_picked_up_order(self, create_blank_order):
        EditOrderPage().set_status('Picked up')
        EditOrderPage().save_order()
        EditOrderPage().green_popup.should(have.text('Load #' + OrderData.load_id + ' saved'))

    def test_add_address_price_and_bol(self, create_blank_order):  # flacky test((
        EditOrderPage().fill_delivery_address(OrderData.delivery_address)
        EditOrderPage().set_status('QuickPay')  # todo find a better way to handle dropdowns
        EditOrderPage().set_price(OrderData.price)
        EditOrderPage().send_bol(OrderData.bol_email)
        EditOrderPage().save_order()
        EditOrderPage().green_popup.should(have.text('Load #' + OrderData.load_id + ' saved'))

    def test_go_to_picked_up(self, create_blank_order):
        EditOrderPage().back_to_orders()
        OrdersPage().go_to_picked_up()
        OrdersPage().click_load_id_of_pickeup_order(create_blank_order)
        ViewOrderPage().load_id.should(have.text(OrderData.load_id))

    def test_edit_pickerd_up_order(self, create_blank_order):
        ViewOrderPage().edit_order()
        EditOrderPage().set_status('Delivered')
        EditOrderPage().delivery_bill_client()
        EditOrderPage().enter_shipper_name(OrderData.shipper_name)
        EditOrderPage().save_contact()
        EditOrderPage().save_order()
        EditOrderPage().green_popup.should(have.text('Load #' + OrderData.load_id + ' saved'))

    def test_search_contact(self, create_blank_order):
        BasePage().open_contacts_page()
        ContactsPage().search_contact(OrderData.shipper_name)
        ContactsPage().contact_name.should(have.text(OrderData.shipper_name))

    def test_remove_contact(self, create_blank_order):
        ContactsPage().remove_contact()  # todo add assertion after delete

    def test_go_to_delivered_and_send_invoice(self, create_blank_order):
        BasePage().open_orders_page()
        OrdersPage().go_to_delivered()
        OrdersPage().send_invoice(create_blank_order, OrderData.bol_email)

    def test_go_to_billed(self, create_blank_order):
        OrdersPage().go_to_billed()
        OrdersPage().edit_billed_orders(create_blank_order)
        EditOrderPage().load_id_input.should(have.value(OrderData.load_id))

    def test_mark_payed_and_cancel(self, create_blank_order):
        EditOrderPage().mark_as_payed_and_cancel()
        EditOrderPage().mark_as_payed_button.should(be.visible)

    def test_archive_order(self, create_blank_order):
        EditOrderPage().archive()
        EditOrderPage().unarchive_button.should(be.visible)

    def test_unarchive_order(self, create_blank_order):
        BasePage().unarchive_order(create_blank_order)
        OrdersPage().go_to_billed()
        OrdersPage().mark_as_payed(create_blank_order, OrderData.payed_amount, OrderData.payment_method)

    def test_go_to_paid_and_delete(self, create_blank_order):
        OrdersPage().go_to_paid()
        OrdersPage().delete_order(create_blank_order)

    def test_go_to_deleted(self, create_blank_order):
        OrdersPage().go_to_deleted()
        DeletedOrdersPage().first_deleted_order.should(have.text(OrderData.load_id))

    def test_select_deleted_order(self, create_blank_order):
        DeletedOrdersPage().select_first_deleted_order()
        DeletedOrdersPage().clear_selection_button.should(be.visible).should(have.text('Clear Selection'))

    def test_clear_selection(self, create_blank_order):
        DeletedOrdersPage().clear_selection()
        DeletedOrdersPage().deleted_header.should(have.exact_text('Deleted Orders'))

    def test_restore_order(self, create_blank_order):
        DeletedOrdersPage().select_first_deleted_order()
        DeletedOrdersPage().restore_order()
        DeletedOrdersPage().green_banner.should(have.exact_text('The selected orders have been restored successfully.'))

    def test_archive_restored_order(self, create_blank_order):
        DeletedOrdersPage().back_to_orders()
        OrdersPage().go_to_paid()

    def test_check_all_order_data(self, create_blank_order):  # todo add checking dates
        ViewOrderPage().open(create_blank_order)
        ViewOrderPage().load_id.should(have.text(OrderData.load_id))
        ViewOrderPage().assigned_to.should(have.text(OrderData.changed_driver_name))
        # ViewOrderPage().invoice_is_sent.should(have.text('Invoice is sent'))
        ViewOrderPage().payment_is_received.should(have.text('Payment is received'))
        ViewOrderPage().first_vin.should(have.exact_text(OrderData.second_vin))
        ViewOrderPage().first_year.should(have.exact_text(OrderData.second_year))
        ViewOrderPage().first_make.should(have.exact_text(OrderData.second_maker))
        ViewOrderPage().first_model.should(have.exact_text(OrderData.second_model))
        ViewOrderPage().first_type.should(have.exact_text('ATV'))
        ViewOrderPage().second_vin.should(have.exact_text(OrderData.vin))
        ViewOrderPage().second_year.should(have.exact_text(OrderData.year))
        ViewOrderPage().second_make.should(have.exact_text(OrderData.maker))
        ViewOrderPage().second_model.should(have.exact_text(OrderData.model))
        ViewOrderPage().second_type.should(have.exact_text('Other'))
        ViewOrderPage().first_inop_label.should(have.exact_text('INOP'))
        ViewOrderPage().origin_address.should(have.text(OrderData.pickup_address))
        ViewOrderPage().destination_address.should(have.text(OrderData.delivery_address))
        ViewOrderPage().shipper.should(have.text(OrderData.delivery_address))
        ViewOrderPage().shipper.should(have.text(OrderData.shipper_name))
        # ViewOrderPage().price.should(have.text(OrderData.price))
        # ViewOrderPage().payment_terms.should(have.text('QuickPay'))
        # ViewOrderPage().payed_amount.should(have.text(OrderData.payed_amount))
        # ViewOrderPage().payment_method.should(have.text('Direct Deposit'))
